# export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "
export PS1="\[\033[32m\]\h:\[\033[33;1m\]\W\[\033[m\] \$ "
export TERM=xterm-color
export GREP_OPTIONS='--color=auto' GREP_COLOR='1;32'
export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad

# LS aliases
alias ls='ls -GFh'
alias ll='ls -alFh'
alias la='ls -Ah'
alias l='ls -CFh'
alias bashrc='source ~/.bashrc'
alias untar="tar -xvzf"
alias cp_folder="cp -Rpv" #copies folder and all sub files and folders, preserving security and dates
alias mirror_website="wget -m -x -p --convert-links --no-host-directories --no-cache -erobots=off"
alias most_used_commands="history | awk '{print \$2}' | awk 'BEGIN {FS=\"|\"}{print \$1}' | sort | uniq -c | sort -n | tail | sort -nr"

alias m='more'
alias df='df -h' # Show disk usage
alias ducks='du -cksh * | sort -rn|head -11' # Lists folders and files sizes in the current folder
alias top='top -o cpu' # os x:

# Added ~/Bin directory as executable path
export PATH=$PATH:$HOME/Bin

# Added by Yahya ERTURAN for NodeJS & NPM
export PATH=$HOME/.npm-global/bin:$PATH

# Enable auto completion after sudo command
complete -cf sudo
